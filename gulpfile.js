// --------------------------------
//  Load modules
// --------------------------------

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var browserify = require('browserify');
var browserSync = require('browser-sync');
var del = require('del');
var jshintStylish = require('jshint-stylish');
var pngquant = require('imagemin-pngquant');
var buffer = require('vinyl-buffer');
var source = require('vinyl-source-stream');
var watchify = require('watchify');

// --------------------------------
//  Path
// --------------------------------

var srcPath = './src';
var destPath = './dest';
var destAssetPath = './dest';
var jsSrcPath = srcPath + '/js/main.js';

// --------------------------------
//  Tasks
// --------------------------------

//
// Sass
//
gulp.task('sass', function () {
  gulp.src(srcPath + '/sass/**/*.scss')
    .pipe($.sass({
      errLogToConsole: true,
      outputStyle: 'expanded'
    })) // Keep running gulp even though occurred compile error
    .pipe($.pleeease({
      autoprefixer: {
        browsers: ['last 2 versions', 'Android 4.4']
      },
      minifier: false
    }))
    .pipe(gulp.dest(destAssetPath + '/css'))
    .pipe(browserSync.reload({stream:true}));
});

//
// JS
//
// jshint
gulp.task('jshint', function() {
  return gulp.src(jsSrcPath)
    .pipe($.plumber())
    .pipe($.jshint())
    .pipe($.jshint.reporter('jshint-stylish'));
});

// browserify
gulp.task('browserify', function() {
  return jscompile(false);
});

// watchify
gulp.task('watchify', function() {
  return jscompile(true);
});

function jscompile(is_watch) {
  var bundler;
  if (is_watch) {
    bundler = watchify(browserify(jsSrcPath));
  } else {
    bundler = browserify(jsSrcPath);
  }

  function rebundle() {
    return bundler
      .bundle()
      .pipe(source('main.js'))
      .pipe(buffer())
      .pipe($.uglify({
        preserveComments: 'some'
      }))
      .pipe(gulp.dest(destAssetPath + '/js'))
      .pipe(browserSync.reload({stream:true}));
  }
  bundler.on('update', function() {
    rebundle();
  });
  bundler.on('log', function(message) {
    console.log(message);
  });
  return rebundle();
}

//
// Imagemin
//
gulp.task('imagemin', function() {
  gulp.src([srcPath + '/images/**/*.{png,jpg,gif}'])
    // .pipe($.imagemin({
    //   optimizationLevel: 7,
    //   progressive: true,
    //   svgoPlugins: [{removeViewBox: false}],
    //   use: [pngquant()]
    // }))
    .pipe(gulp.dest(destAssetPath + '/images'));
});

//
// SVG
//
gulp.task('svg-sprite', function () {
  gulp.src([srcPath + '/svg/icons/*.svg'])
    .pipe($.svgmin())
    .pipe($.svgstore({ inlineSvg: true }))
    .pipe($.cheerio({
      run: function ($, file) {
          $('svg').addClass('hide');
          $('[fill]').removeAttr('fill');
      },
      parserOptions: { xmlMode: true }
    }))
    .pipe(gulp.dest(destAssetPath + '/images'));
});

gulp.task('svgmin', function() {
  gulp.src([srcPath + '/images/*.{svg}'])
    .pipe($.svgmin())
    .pipe($.cheerio({
      run: function ($, file) {
          $('svg').addClass('hide');
          $('[fill]').removeAttr('fill');
      },
      parserOptions: { xmlMode: true }
    }))
    .pipe(gulp.dest(destAssetPath + '/images'));
});

//
// ejs
//
var fs = require('fs');
var json = JSON.parse(fs.readFileSync('site.json')); // parse json
gulp.task('ejs', function() {
  gulp.src([srcPath + '/templates/**/*.ejs','!' + srcPath + '/templates/_*.ejs']) // Don't build html which starts from underline
    .pipe($.plumber())
    .pipe($.ejs(json, {}, {
      'ext': '.html'
    }))
    // .pipe($.minifyHtml({
    //   conditionals: true
    // }))
    .pipe(gulp.dest(destPath + '/'));
});

//
// Copy
//
gulp.task('assets', function() {
  gulp.src(srcPath + '/www/**/*')
    .pipe($.plumber())
    .pipe(gulp.dest(destPath + '/'))
    .pipe(browserSync.reload({stream:true}));
});

//
// Static server
//
gulp.task('browser-sync', function() {
  browserSync({
    server: {
      baseDir: destPath, // Target directory
      index: 'index.html' // index file
    },
    open: false,
    reloadDelay: 500
  });
});

//
// Reload all browsers
//
gulp.task('bs-reload', function () {
  browserSync.reload();
});

//
// Clean
//
gulp.task('clean', function() {
  del(destPath);
});

//
// Build
//
gulp.task('build', ['sass', 'jshint', 'browserify', 'imagemin', 'svg-sprite', 'svgmin', 'ejs', 'assets']);

//
// Task for `gulp` command
//
gulp.task('default',['browser-sync', 'watchify'], function() {
  gulp.watch(srcPath + '/sass/**/*.scss',['sass']);
  gulp.watch(srcPath + '/images/**/*.{png,jpg,gif,svg}',['imagemin']);
  gulp.watch(srcPath + '/svg/**/*.svg',['svg-sprite', 'svgmin']);
  gulp.watch(jsSrcPath,['jshint']);
  gulp.watch(srcPath + '/www/**/*', ['assets']);
  gulp.watch(destPath + '/**/*.html', ['bs-reload']);
  gulp.watch([srcPath + '/templates/**/*.ejs', 'site.json'], ['ejs']);
});
