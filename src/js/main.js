(function() {
  'use strict';

  //
  // Require
  //
  var $ = require('jquery');
  global.jQuery = require('jquery');

  // Retina images
  $(function() {
    // Retinaディスプレイの時だけ実行
    if(window.devicePixelRatio > 1) {
      $('#body').addClass('is-retina');
      // img要素を探す
      $('.js-retina-image').each(function() {
        // src取得
        var src = $(this).attr('src');
        // 高解像度画像に置換
        $(this).attr('src', src.replace(/(.jpg|.png)/gi,'@2x$1')).error(function(){
          $(this).attr('src', src); // @2xの画像がない場合は元画像を表示
        });
      });
    }
  });

  //
  // Smooth scroll
  //
  $('.js-smooth-scroll').on('click', function(e) {
    e.preventDefault();
    var speed = 400;
    var adjustH = 0;
    var href= $(this).attr('href');
    var target = $(href === '#' || href === '' ? 'html' : href);
    var position = target.offset().top - adjustH;
    if(position < 0) {
      position = 0;
    }
    $('html, body').animate({scrollTop:position}, speed, 'swing');
  });

  // Responsive
  $(function() {
    var spFlg = false;

    $(window).on('load resize orientationchange', function() {

      if ($('#spFlg').is(':visible')) {
        spFlg = true;

        $('.js-responsive-image').each(function() {
          $(this).attr('src',$(this).attr('src').replace('_pc', '_sp'));
        });
      } else {
        spFlg = false;

        $('.js-responsive-image').each(function() {
          $(this).attr('src',$(this).attr('src').replace('_sp', '_pc'));
        });
      }
    });
  });

  // Accordion
  $(function() {
    $('#js-toggle-menu').on('click', function(e) {
      e.preventDefault();
      if($(this).hasClass('is-open')) {
        $(this).removeClass('is-open');
        $('#js-header-menu').removeClass('is-open');
      } else {
        $(this).addClass('is-open');
        $('#js-header-menu').addClass('is-open');
      }
    });

    $('.js-smooth-scroll').on('click', function(e) {
      e.preventDefault();
      $('#js-toggle-menu').removeClass('is-open');
      $('#js-header-menu').removeClass('is-open');
    });
  });

  $(function() {
    $('.js-toggle-dropdown').on('click', function(e) {
      e.preventDefault();
      var $dropdownList = $(this).parent().children('.js-dropdown-list');
      if($(this).hasClass('is-open')) {
        $(this).removeClass('is-open');
        $dropdownList.removeClass('is-open');
      } else {
        $(this).addClass('is-open');
        $dropdownList.addClass('is-open');
      }
    });
  });

  // Scroll effects
  $(window).on('load scroll', function () {
    var scroll = $(window).scrollTop();
    var wH = $(window).height();
    var headerH = $('#header').outerHeight();
    var adjustH = 60;

    // if ($('#spFlg').is(':visible')) {
    //   adjustH = 60;
    // }

    var scrollPos = scroll + wH - adjustH;

    $('.js-scroll-view').each(function() {
      var imgPos = $(this).offset().top;

      if (scrollPos > imgPos){
        $(this).addClass('is-animated');
      }
    });

    if(scroll > 0) {
      $('#body').removeClass('is-top');
      if (scroll > headerH) {
        $('#header').addClass('is-fixed');
      }
    } else {
      $('#header').removeClass('is-fixed');
      $('#body').addClass('is-top');
    }
  });
})();
